'use strict';
var path = require('path');
var serve = require('serve-static');

var app = module.exports = require('express')();
app.use(require('body-parser').json()); // for parsing application/json
app.set('port', (process.env.PORT || 3000));

app.use('/', serve(path.join(__dirname, '..', 'static')));
app.use('/vendor', serve(path.join(__dirname, '..', 'bower_components')));
app.use('/data', serve(path.join(__dirname, '..', 'data', 'base')));

app.listen(app.get('port'), function runServer () {
  console.log('Server running at http://localhost:' + app.get('port'));
});
