'use strict';
const gulp = require('gulp');
const jshint = require('gulp-jshint');
const nodemon = require('gulp-nodemon');

// file paths relative to project root
const entry = 'server/server.js';
const src = [entry, 'server/**/*.js'];

gulp.task('server-lint', [], function () {
  return gulp.src(src).pipe(jshint({
    esversion: 6,
    node: true
  }))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('server', ['server-lint'], function() {
  nodemon({
    script: entry,
    ext: 'js',
    watch: src,
    tasks: ['server-lint']
  }).on('restart', function () {
    console.log('restarted!');
  });
});
