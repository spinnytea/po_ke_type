'use strict';
// scratchpad for running the data scripts
// we need to be able to just run stuff until we are stable enough to write an init script

parseTypes();
parsePokemon();
findBootswatch();


// utility functions
function logResolve(data) { console.log(data); return data; }
function logReject(data) { console.error(data); return Promise.reject(data); }

//

function downloadTypes() {
  return require('./scripts/load_page')('./data/raw/pokemon_db_types', 'http://pokemondb.net/type')
    .then(function() { return 'downloaded!'; })
    .then(logResolve, logReject);
}

function parseTypes() {
  return require('./scripts/pokemon_db_types__1').process('./data')
    .then(logResolve, logReject);
}

function parsePokemon() {
  return require('./scripts/pokemon_db_pokedex__1').process('./data')
    .then(logResolve, logReject);
}

function findBootswatch() {
  return require('./scripts/bootswatch').process('./data', './bower_components/bootswatch')
    .then(logResolve, logReject);
}
