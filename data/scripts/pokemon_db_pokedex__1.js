'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');
const lp = require('./load_page');

exports.process = function(path_to_data) {
  return lp(path.join(path_to_data, 'raw', 'pokemon_db_pokedex'), 'http://pokemondb.net/pokedex/all').then(function(data) {
    let $ = cheerio.load(data);
    let table = $('#pokedex.data-table');
    if(table.length !== 1) return Promise.reject('Could not find pokedex table in data');
    if(table.find('thead th').length !== 10) return Promise.reject('pokedex table columns have changed');
    const expectedHeaders = ['#','Name','Type','Total','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed'];
    const actualHeaders = table.find('thead th')
    	.map(function(idx, elem) { return $(elem).text(); })
    	.toArray();
    if(!_.isEqual(expectedHeaders, actualHeaders)) return Promise.reject('pokedex table column order has changed to ' + JSON.stringify(actualHeaders));

    let pokedex = table.find('tbody tr').map(function(r, row) {
    	row = $(row);
    	return {
    		iconClass: row.find('td:nth-child(1) i').attr('data-sprite').split(' ')[1],
    		number: +row.find('td:nth-child(1)').text().trim(),
    		name: row.find('td:nth-child(2) a').text().trim(),
    		specialname: row.find('td:nth-child(2) small').text().trim(),
    		types: row.find('td:nth-child(3) a').map(function(ignore, elem) { return $(elem).text().toLowerCase(); }).toArray(),
    	};
    }).toArray();

    return new Promise(function(resolve, reject) {
      fs.writeFile(path.join(path_to_data, 'base', 'pokedex.json'), JSON.stringify(pokedex, null, 2), { encoding: 'utf8' }, function(err) {
        if(err) return reject(err);
        resolve('finished parsing pokedex');
      });
    });
  });
};
