'use strict';
var fs = require('fs');
var path = require('path');
// we need to build a list of themes from the ones we have available
// this use to be in a REST layer, but we want it to be static now

exports.process = function(path_to_data, root) {
  var themes = fs.readdirSync(root).filter(function(filename) {
    // this is the path we are using in the html file
    return fs.existsSync(path.join(root, filename, 'bootstrap.min.css'));
  });

  return new Promise(function(resolve, reject) {
    fs.writeFile(path.join(path_to_data, 'base', 'themes.json'), JSON.stringify(themes, null, 2), { encoding: 'utf8' }, function(err) {
      if(err) return reject(err);
      resolve('finished finding themes');
    });
  });
};

