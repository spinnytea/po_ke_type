'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');
const lp = require('./load_page');

exports.process = function(path_to_data) {
  return lp(path.join(path_to_data, 'raw', 'pokemon_db_types'), 'http://pokemondb.net/type').then(function(data) {
    let $ = cheerio.load(data);
    let typeTable = $('.type-table');
    if(typeTable.length !== 1) return Promise.reject('Could not find type-table in data');
    if(typeTable.find('thead th').length !== 19) return Promise.reject('type-table columns have changed');
    if(typeTable.find('tbody tr').length !== 18) return Promise.reject('type-table rows have changed');

    // parse the types
    let types = [];
    typeTable.find('tbody tr th').each(function(idx, elem) {
      let typeName = $(elem).text().toLowerCase();
      types.push(typeName);
    });
    if(types.length !== 18) return Promise.reject('error while parsing types in header');
    // verify that the headers are in the same order
    if(!_.isEqual(types, typeTable.find('thead th a').map(function(idx, elem) {
      return $(elem).attr('title').toLowerCase();
    }).toArray())) return Promise.reject('thead headers not in same order as tbody headers');

    // initialize the multipliers to 1
    let multipliers = {};
    types.forEach(function(attacker) {
      multipliers[attacker] = {};
      types.forEach(function(defender) {
        multipliers[attacker][defender] = 1;
      });
    });

    // parse the multipliers
    const expectedClasses = ['type-fx-cell', 'type-fx-0', 'type-fx-50', 'type-fx-100', 'type-fx-200'];
    const actualClasses = [];
    typeTable.find('tbody tr').each(function(rowIdx, row) { // attackers
      const attacker = types[rowIdx];
      $(row).find('td').each(function(colIdx, cell) { // defenders
        const defender = types[colIdx];
        cell = $(cell);
        Array.prototype.push.apply(actualClasses, cell.attr('class').split(' '));

        if(cell.hasClass('type-fx-0')) {
          multipliers[attacker][defender] = 0;
        } else if(cell.hasClass('type-fx-50')) {
          multipliers[attacker][defender] = 0.5;
        } else if(cell.hasClass('type-fx-200')) {
          multipliers[attacker][defender] = 2;
        }
      });
    });
    // verify that it's using the classes we are looking for
    if(!_.isEqual(expectedClasses.sort(), _.uniq(actualClasses).sort())) return Promise.reject('the td classes have changed');

    return new Promise(function(resolve, reject) {
      fs.writeFile(path.join(path_to_data, 'base', 'types.json'), JSON.stringify(multipliers, null, 2), { encoding: 'utf8' }, function(err) {
        if(err) return reject(err);
        resolve('finished parsing types');
      });
    });
  });
};
