'use strict';
const fork = require('child_process').fork;
const gulp = require('gulp');
const jshint = require('gulp-jshint');

require('./client/gulpfile');
require('./server/gulpfile');

gulp.task('dev', ['server', 'buildd']);

gulp.task('script-lint', [], function () {
  // Note: do not lint scratch
  return gulp.src('./data/scripts/**/*.js').pipe(jshint({
    esversion: 6,
    node: true
  }))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});
gulp.task('scratch', ['script-lint'], function(done) {
  fork('./data/scratch', {silent: false}).on('exit', ()=>done());
});
gulp.task('scratchd', [], function() {
  gulp.watch('./data/**/*.js', ['scratch']);
  gulp.start('scratch');
});

gulp.task('data', [], function() {
  function logResolve(data) { console.log(data); return data; }
  function logReject(data) { console.error(data); return Promise.reject(data); }
  return Promise.all([
    require('./data/scripts/pokemon_db_types__1').process('./data').then(logResolve, logReject),
    require('./data/scripts/pokemon_db_pokedex__1').process('./data').then(logResolve, logReject),
    require('./data/scripts/bootswatch').process('./data', './bower_components/bootswatch'),
  ]);
});
