#!/usr/bin/env bash
set -x
npm install
node_modules/.bin/bower install
node_modules/.bin/gulp data
node_modules/.bin/gulp build

set +x
echo ""
echo "Now that everything installed and initialized, all you have left is to run it"
echo " - start the server with \"node_modules/.bin/gulp server\""
echo " - open a browser and nav to localhost:3000"
echo ""
