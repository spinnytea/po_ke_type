'use strict';
var _ = require('lodash');
module.exports = exports = angular.module('po_ke_type.pokedex', []);

exports.controller('po_ke_type.pokedex.team.controller', [
  '$scope', '$q', 'bindKeys', 'localStorageService', 'po_ke_type.pokedex.list', 'po_ke_type.pokedex.team',
  function TeamController($scope, $q, bindKeys, localStorageService, dexPromise, teamPromise) {
    $scope.colorfulCards = localStorageService.get('colorfulCards');
    $scope.nested = { filter: '', filteredDex: undefined };
    $scope.showList = function() { return $scope.nested.filter.length > 1; };

    $scope.isInTeam = function(mon) {
      return _.includes($scope.team, mon);
    };
    $scope.addToTeam = function(mon) {
      if(!_.includes($scope.team, mon)) $scope.team.push(mon);
    };
    $scope.removeFromTeam = function(mon) {
      _.pull($scope.team, mon);
    };

    function getFilteredList() {
      // get the currently filtered list
      var list = $scope.showList()?$scope.nested.filteredDex:[];

      // check for exact matches (there aren't many)
      var filter = $scope.nested.filter.toLowerCase();
      var exactList = list.filter(function(mon) { return mon.name.toLowerCase() === filter; });
      if(exactList.length) return exactList;

      // if no exact matches, then return the full list
      return list;
    }
    $scope.addAll = function() {
      getFilteredList().forEach($scope.addToTeam);
    };
    $scope.removeAll = function() {
      getFilteredList().forEach($scope.removeFromTeam);
    };
    bindKeys($scope, {
      'enter': $scope.addAll,
      'esc': $scope.removeAll,
    });

    $scope.loading = $q.all({
      dex: dexPromise,
      team: teamPromise,
    }).then(function(loadedData) {
      _.assign($scope, loadedData);
    });
  }
]);

exports.controller('po_ke_type.pokedex.controller', [
  '$scope', '$q', 'po_ke_type.pokedex.list',
  function PokedexController($scope, $q, dexPromise) {
    $scope.nested = { filter: '', limit: 20 };

    $scope.getUrl = function(mon) {
      return '#/pokedex/' + mon.name + (mon.specialname?('/'+encodeURI(mon.specialname)):'');
    };

    $scope.loading = $q.all({
      dex: dexPromise,
    }).then(function(loadedData) {
      _.assign($scope, loadedData);
    });
  }
]);

exports.controller('po_ke_type.pokedex.pokemon.controller', [
  '$scope', '$q', '$routeParams', 'po_ke_type.pokedex.list', 'po_ke_type.types.chart', 'po_ke_type.pokedex.team',
  function PokedexPokemonController($scope, $q, $routeParams, dexPromise, typeChartPromise, teamPromise) {
    var name = ($routeParams.name || '').toLowerCase();
    var specialname = (decodeURI($routeParams.specialname || '')).toLowerCase();
    // show evolutions? no, not unless we can get a source that's super succinct
    // attack types? no, this gets complicated very quickly

    $scope.loading = $q.all({
      dex: dexPromise,
      typeChart: typeChartPromise,
      team: teamPromise,
    }).then(function(loadedData) {
      _.assign($scope, loadedData);
      $scope.mon = _.find($scope.dex, function(mon) {
        return mon.name.toLowerCase() === name && mon.specialname.toLowerCase() === specialname;
      });
      if(!$scope.mon) return $q.reject('bad path');

      // when MON is attacking
      $scope.attacking = $scope.team.map(function(m) {
        var rate = calculateMaxDamageRate($scope.mon, m, $scope.typeChart);
        return _.assign({ rate: rate }, m);
      });
      // when MON is defending
      $scope.defending = $scope.team.map(function(m) {
        var rate = calculateMaxDamageRate(m, $scope.mon, $scope.typeChart);
        return _.assign({ rate: rate }, m);
      });
    });
  }
]);

exports.factory('po_ke_type.pokedex.team', [
  'localStorageService', '$rootScope', '$q', 'po_ke_type.pokedex.list',
  function(localStorageService, $rootScope, $q, dexPromise) {
    var key = 'team_list';
    var list = localStorageService.get(key);

    var promise;
    if(list) {
      // restore team list
      promise = dexPromise.then(function(dex) {
        list = list.map(function(m) { return _.find(dex, m); });
        return list; // the team list is the result of the promise
      });
    } else {
      list = [];
      promise = $q.resolve(list);
    }

    $rootScope.$watch(function() { return list.length; }, function() {
      if(list.length) {
        // we only need to store the name and special name
        localStorageService.set(key, list.map(function(mon) {
          return _.pick(mon, 'name', 'specialname');
        }));
      } else {
        // if the list is empty, then we can delete the data
        localStorageService.remove(key);
      }
    });
    $rootScope.$on('LocalStorageModule.notification.removeitem', function(event, args) {
      if(args.key === key) {
        list.splice(0);
      }
    });

    return promise;
  }
]);

exports.factory('po_ke_type.pokedex.list', [ '$q', function($q) {
  return $q(function(resolve) {
    $.getJSON('data/pokedex.json', function(dex) {
      dex.forEach(function(mon) {
        mon.typeStyle = mon.types[0];
        if(mon.types[1]) mon.typeStyle += '-' + mon.types[1];
      });
      resolve(dex);
    });
  });
}]);

exports.filter('dexGen', ['localStorageService', function(localStorageService) {
  // (gen 1 is position 1)
  var MAX_NUM = [0, 151, 251, 386, 494, 649, 721];
  return function DexGenFilter(array) {
    var gen = +(localStorageService.get('dexGen') || '6');
    var maxNum = MAX_NUM[gen];
    return array.filter(function(mon) {
      // filter out mega evolutions unless we are Gen VI forward
      if(gen < 6 && mon.specialname.indexOf('Mega') === 0) return false;
      return mon.number <= maxNum;
    });
  };
}]);

exports.directive('bulbapedia', [function() {
  return {
    restrict: 'A',
    replace: 'true',
    scope: { mon: '=bulbapedia' },
    template: '<a ng-href="http://bulbapedia.bulbagarden.net/wiki/{{mon.name}}_(Pok%C3%A9mon)" target="bulbapedia"><i class="fa fa-external-link"></i></a>',
  };
}]);

exports.directive('pokemonPill', [function() {
  return {
    restrict: 'A',
    replace: true,
    scope: { mon: '=pokemonPill' },
    templateUrl: 'pokedex/pokemon-pill.html',
    controller: ['$scope', 'localStorageService', PokemonPillController]
  };

  function PokemonPillController($scope, localStorageService) {
    $scope.colorfulCards = localStorageService.get('colorfulCards');
    $scope.getUrl = function(mon) {
      return '#/pokedex/' + mon.name + (mon.specialname?('/'+encodeURI(mon.specialname)):'');
    };
  }
}]);

function calculateMaxDamageRate(atk, def, chart) {
  return atk.types.reduce(function(max, atk_type) {
    var rate = def.types.reduce(function(r, def_type) {
      return r * chart[atk_type][def_type];
    }, 1);
    return Math.max(max, rate);
  }, 0);
}
