'use strict';
var _ = require('lodash');
module.exports = exports = angular.module('po_ke_type.types', []);

exports.filter('rateDisplay', function() {
  return function rateDisplay(input) {
    if(!_.isNumber(input)) return '';

    switch(input) {
      case 1: return ''; // don't display 1s
      case 0.5: return '½'; // fancy
      case 0.25: return '¼'; // fancy
      default: return input; // expected 0, 2, 4
    }
  };
});
exports.filter('rateStyle', function() {
  return function rateStyle(input, inverse) {
    if(!_.isNumber(input)) return '';

    if(inverse === true || inverse === 'inv' || inverse === 'inverse') {
      if(input !== 0) input = 1/input;
    }

    return 'rate-' + input*100;
  };
});

exports.directive('typeSquare', [function() {
  return {
    restrict: 'A',
    replace: true,
    scope: { type: '=typeSquare' },
    templateUrl: 'types/square.html',
    controller: ['$scope', 'po_ke_type.types.list', 'po_ke_type.types.chart', TypeSquareController]
  };

  function TypeSquareController($scope, typeListPromise, typeChartPromise) {
    typeListPromise.then(function(typeList) {
      typeChartPromise.then(function(typeChart) {
        $scope.dmgTo200 = typeList.filter(function(t) { return typeChart[$scope.type][t] === 2; });
        $scope.dmgTo50 = typeList.filter(function(t) { return typeChart[$scope.type][t] === 0.5; });
        $scope.dmgTo0 = typeList.filter(function(t) { return typeChart[$scope.type][t] === 0; });

        $scope.dmgFrom200 = typeList.filter(function(t) { return typeChart[t][$scope.type] === 2; });
        $scope.dmgFrom50 = typeList.filter(function(t) { return typeChart[t][$scope.type] === 0.5; });
        $scope.dmgFrom0 = typeList.filter(function(t) { return typeChart[t][$scope.type] === 0; });

        $scope.text = typeList.reduce(function(ret, type) {
          ret[type] = {
            name: type.charAt(0).toUpperCase() + type.substr(1).toLowerCase(),
            title: type.substr(0,3).toUpperCase(),
          };
          return ret;
        }, {});
      });
    });
  }
}]);

exports.controller('po_ke_type.types.typeChartController', [
  '$scope', '$q', 'localStorageService', 'po_ke_type.types.list', 'po_ke_type.types.chart',
  function TypeChartController($scope, $q, localStorageService, typeListPromise, typeChartPromise) {
    $scope.whichChart = localStorageService.get('preferredTypeChart') || 'matrix';

    $scope.loading = $q.all({
      typeList: typeListPromise,
      typeChart: typeChartPromise,
    }).then(function(loadedData) {
      _.assign($scope, loadedData);

      $scope.text = $scope.typeList.reduce(function(ret, type) {
        ret[type] = {
          name: type.charAt(0).toUpperCase() + type.substr(1).toLowerCase(),
          title: type.substr(0,3).toUpperCase(),
        };
        return ret;
      }, {});
    });
  }
]);

exports.factory('po_ke_type.types.list', [ 'po_ke_type.types.chart', function(typeChartPromise) {
  return typeChartPromise.then(function(typeChart) { return _.keys(typeChart); });
}]);
exports.factory('po_ke_type.types.chart', [ '$q', function($q) {
  return $q(function(resolve) {
    $.getJSON('data/types.json', resolve);
  });
}]);
