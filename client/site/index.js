'use strict';
var _ = require('lodash');
// some high-level stuff
// manages the overall site

module.exports = exports = angular.module('po_ke_type.site', []);

exports.controller('po_ke_type.site.home.controller', [
  '$scope',
  'po_ke_type.pokedex.list', 'po_ke_type.types.list', 'po_ke_type.types.chart', 'po_ke_type.pokedex.team',
  function($scope, dexPromise, typeListPromise, typeChartPromise, teamPromise) {
    $scope.dexPromise = dexPromise;
    $scope.typeListPromise = typeListPromise;
    $scope.typeChartPromise = typeChartPromise;
    $scope.teamPromise = teamPromise.then(function(team) { return ($scope.team = team); });
  }
]);

exports.directive('promiseStatus', function() {
  return {
    restrict: 'A',
    scope: { promise: '=promiseStatus', text: '@' },
    template: '' +
      '<span ng-if="status === \'pending\'"><i class="fa fa-fw fa-refresh fa-spin"></i>Loading <span ng-bind="text"></span>...</span>' +
      '<span ng-if="status === \'resolve\'"><i class="fa fa-fw fa-check text-success"></i>Loaded <span ng-bind="text"></span>.</span>' +
      '<span ng-if="status === \'reject\'"><i class="fa fa-fw fa-remove text-danger"></i><span ng-bind="text"></span> not loaded.</span>' +
    '',
    controller: ['$scope', function($scope) {
      $scope.status = 'pending';
      $scope.promise.then(function() { $scope.status = 'resolve'; }, function() { $scope.status = 'reject'; });
    }],
  };
});

exports.directive('pageHeader', [
  function() {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'site/pageHeader.html',
      controller: [
        '$scope', '$location',
        PageHeaderController
      ]
    };

    function PageHeaderController($scope, $location) {
      $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
      };
    }
  }
]);

exports.directive('loading', [
  function() {
    return {
      restrict: 'A',
      transclude: true,
      scope: { promise: '=loading' },
      templateUrl: 'site/loading.html',
      controller: ['$scope', LoadingController]
    };

    function LoadingController($scope) {
      $scope.reload = function() { location.reload(); };
      $scope.loading = true;
      $scope.promise
        .then(function() { $scope.loading = false; })
        .catch(function(code) { $scope.loading = code || 'failed'; });
    }
  }
]);

exports.controller('po_ke_type.site.settings.controller', [
  '$scope', '$q', 'localStorageService',
  function SettingsController($scope, $q, localStorageService) {
    // when we clear the local storage, we need to reset the local values
    $scope.defaults = {};

    // get themes from server
    $scope.themes = [ $scope.html.theme ]; // seed the list so the user doesn't notice
    $.getJSON('data/themes.json').then(function(list) { $scope.themes = list; }); // fill in the full list once we have it
    $scope.saveTheme = function() {
      localStorageService.set('theme', $scope.html.theme);
    };

    setupGetterSetter('preferredTypeChart', 'matrix');
    setupGetterSetter('dexGen', '6');
    setupGetterSetter('colorfulCards', false);

    function setupGetterSetter(name, defaultValue) {
      $scope.defaults[name] = defaultValue;
      $scope[name] = localStorageService.get(name) || defaultValue;
      $scope['save_' + name] = function() {
        localStorageService.set(name, $scope[name]);
      };
    }

    $scope.clearLocalStorage = function() {
      _.assign($scope, $scope.defaults);
      return localStorageService.clearAll();
    };
  }
]);

var DEFAULT_THEME = 'flatly';
exports.controller('po_ke_type.site.html.controller', [
  '$rootScope', 'localStorageService',
  function($rootScope, localStorageService) {
    var controller = this;
    controller.theme = localStorageService.get('theme') || DEFAULT_THEME;
    $rootScope.$on('LocalStorageModule.notification.removeitem', function(event, args) {
      if(args.key === 'theme') {
        controller.theme = DEFAULT_THEME;
      }
    });
  }
]);
