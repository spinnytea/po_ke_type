'use strict';

module.exports = exports = angular.module('po_ke_type', [
  require('./pokedex').name,
  require('./site').name,
  require('./types').name,

  require('./utils').name,
  'templates',
  'LocalStorageModule',
  'ngRoute'
]);

exports.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'site/home.html',
    controller: 'po_ke_type.site.home.controller'
  }).when('/team', {
    templateUrl: 'pokedex/team.html',
    controller: 'po_ke_type.pokedex.team.controller'
  }).when('/pokedex', {
    templateUrl: 'pokedex/pokedex.html',
    controller: 'po_ke_type.pokedex.controller'
  }).when('/pokedex/:name/:specialname?', {
    templateUrl: 'pokedex/pokemon.html',
    controller: 'po_ke_type.pokedex.pokemon.controller'
  }).when('/types', {
    templateUrl: 'types/typeChart.html',
    controller: 'po_ke_type.types.typeChartController'
  }).when('/settings', {
    templateUrl: 'site/settings.html',
    controller: 'po_ke_type.site.settings.controller'
  }).otherwise({
    templateUrl: 'oops.html'
  });
}]);
exports.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('po_ke_type')
    .setNotify(false, true);
}]);
