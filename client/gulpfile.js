'use strict';
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var cleanCSS = require('gulp-clean-css');
var gulp = require('gulp');
var gutil = require('gutil');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var templateCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');


// file paths relative to project root
var entry = 'client/index.js';
var outputName = 'index.js';
var js = [ entry, 'client/**/*.js' ];
var html = [ 'client/**/*.html' ];
var css = [ 'client/**/*.less', 'client/**/*.css' ];


gulp.task('build-js', ['lint'], function () {
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: entry,
    debug: true
  });

  return b.bundle()
    .pipe(source(outputName))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('static'));
});
gulp.task('lint', [], function () {
  return gulp.src(js).pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('build-html', [], function() {
  return gulp.src(html)
    .pipe(templateCache({ standalone: true }))
    .pipe(gulp.dest('static'));
});

gulp.task('build-css', [], function() {
  return gulp.src('client/site.less')
    .pipe(sourcemaps.init())
    .pipe(less({ paths: css }))
    .pipe(cleanCSS())
    .on('error', function() { gutil.log(arguments); this.emit('end'); })
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('static'));
});


gulp.task('build', ['build-js', 'build-html', 'build-css'], function() {});
gulp.task('buildd', [], function() {
  gulp.watch(js, ['build-js']);
  gulp.watch(html, ['build-html']);
  gulp.watch(css, ['build-css']);
  gulp.start('build');
});
