Overview
========
Pokemon type advantage calculator. Sure you can do it in your head, but computers are here to help!

Getting Started
===============
Run `setup.sh`
Run `gulp server`
Launch http://localhost:3000

What's the point
================
The [Type Chart](http://localhost:3000/#/types) is a useful reference for knowing which pokémon to send into battle.
But why stop there? Head over to [Your Team](http://localhost:3000/#/team) and select the pokémon you are bringing with you or the ones you plan to take, then see how they match up with other pokémon in the [Pokédex](http://localhost:3000/#/pokedex).


Project Model
=============
* https://bitbucket.org/spinnytea/crafter_life
* https://github.com/spinnytea/spinningnode

Data Sources
============
* http://pokemondb.net

References
==========
* http://pokemondb.net
* http://bulbapedia.bulbagarden.net/wiki/Main_Page
* http://pldh.net/
